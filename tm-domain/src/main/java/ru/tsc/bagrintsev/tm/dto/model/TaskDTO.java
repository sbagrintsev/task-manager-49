package ru.tsc.bagrintsev.tm.dto.model;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractWBSModelDTO {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
