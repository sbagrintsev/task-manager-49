package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskClearResponse extends AbstractTaskResponse {

}
