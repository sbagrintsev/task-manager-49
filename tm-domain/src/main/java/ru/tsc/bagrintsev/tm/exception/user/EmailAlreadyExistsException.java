package ru.tsc.bagrintsev.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public final class EmailAlreadyExistsException extends AbstractUserException {

    public EmailAlreadyExistsException(@NotNull final String email) {
        super(String.format("Error! Email %s is already in use...", email));
    }

}
