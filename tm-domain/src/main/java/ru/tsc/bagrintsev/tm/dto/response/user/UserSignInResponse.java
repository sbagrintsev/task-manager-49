package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserSignInResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserSignInResponse(@Nullable String token) {
        this.token = token;
    }

    public UserSignInResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
