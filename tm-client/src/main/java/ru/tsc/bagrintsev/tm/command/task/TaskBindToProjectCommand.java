package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.PROJECT_ID);
        @Nullable final String projectId = TerminalUtil.nextLine();
        showParameterInfo(EntityField.TASK_ID);
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

}
