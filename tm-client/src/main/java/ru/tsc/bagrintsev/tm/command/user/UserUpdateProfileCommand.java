package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserUpdateProfileRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.FIRST_NAME);
        @NotNull final String firstName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.MIDDLE_NAME);
        @NotNull final String middleName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.LAST_NAME);
        @NotNull final String lastName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        request.setLastName(lastName);
        getUserEndpoint().updateProfile(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
