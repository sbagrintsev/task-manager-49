package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskListByProjectIdResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.PROJECT_ID);
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        int index = 0;
        for (final TaskDTO task : tasks) {
            System.out.println(++index + ". " + task);
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List tasks by project id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

}
