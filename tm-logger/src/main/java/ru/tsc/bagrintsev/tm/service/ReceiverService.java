package ru.tsc.bagrintsev.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IReceiverService;
import ru.tsc.bagrintsev.tm.listener.LoggerListener;

import javax.jms.*;

public class ReceiverService implements IReceiverService {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    private static final String QUEUE = "TM_QUEUE";

    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @Override
    @SneakyThrows
    public void receive(@NotNull final LoggerListener listener) {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createQueue(QUEUE);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
