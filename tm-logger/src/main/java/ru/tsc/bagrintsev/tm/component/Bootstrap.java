package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;
import ru.tsc.bagrintsev.tm.api.sevice.IReceiverService;
import ru.tsc.bagrintsev.tm.listener.LoggerListener;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.LoggerService;
import ru.tsc.bagrintsev.tm.service.ReceiverService;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap {

    public void run() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
        factory.setTrustAllPackages(true);

        @NotNull final IReceiverService receiverService = new ReceiverService();
        @NotNull final ConnectionService connectionService = new ConnectionService();
        @NotNull final ILoggerService loggerService = new LoggerService(connectionService);
        @NotNull LoggerListener loggerListener = new LoggerListener(loggerService);

        receiverService.receive(loggerListener);
    }

}
