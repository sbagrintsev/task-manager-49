package ru.tsc.bagrintsev.tm.service;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;

@Getter
@Setter
@NoArgsConstructor
public class ConnectionService implements IConnectionService {

    @NotNull
    final MongoClient mongoClient = new MongoClient("localhost", 27017);

    @NotNull
    final MongoDatabase mongoDatabase = mongoClient.getDatabase("task-manager");

}
