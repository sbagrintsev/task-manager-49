package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.List;

public class UserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public UserOwnedRepository(
            @NotNull final Class<M> clazz,
            @NotNull final EntityManager entityManager
    ) {
        super(clazz, entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.user.id = :userId", clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.id = :id AND m.user.id = :userId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public @Nullable List<M> findAllByUserId(@NotNull final String userId) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.user.id = :userId", clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.id = :id AND m.user.id = :userId",
                clazz.getSimpleName());
        @NotNull final List<M> result = entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        entityManager.remove(findOneById(userId,id));
    }

    @Override
    public long totalCountByUserId(@NotNull final String userId) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) FROM %s m WHERE m.user.id = :userId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void update(@NotNull final M record) {
        entityManager.merge(record);
    }

}
