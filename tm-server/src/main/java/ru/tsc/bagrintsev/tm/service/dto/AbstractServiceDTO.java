package ru.tsc.bagrintsev.tm.service.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAbstractServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IAbstractRepositoryDTO<M>> implements IAbstractServiceDTO<M> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    public abstract long totalCount();

}
