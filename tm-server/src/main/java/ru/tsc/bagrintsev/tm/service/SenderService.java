package ru.tsc.bagrintsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.Table;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.ISenderService;
import ru.tsc.bagrintsev.tm.dto.EventMessage;

import javax.jms.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SenderService implements ISenderService {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;


    @NotNull
    private static final String QUEUE = "TM_QUEUE";

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Destination destination;

    @NotNull
    private final MessageProducer producer;

    @SneakyThrows
    public SenderService(@NotNull final BrokerService broker) {
        initJMS(broker);
        this.connection = connectionFactory.createConnection();
        connection.start();
        this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.destination = session.createQueue(QUEUE);
        this.producer = session.createProducer(destination);
    }

    @Override
    public void createMessage(
            @NotNull final Object object,
            @NotNull final String eventType
    ) {
        executorService.submit(() -> sync(object, eventType));
    }

    public void initJMS(@NotNull final BrokerService broker) throws Exception {
        broker.addConnector(ActiveMQConnectionFactory.DEFAULT_BROKER_BIND_URL);
        broker.start();
    }

    private void send(@NotNull final String json) throws JMSException {
        final TextMessage message = session.createTextMessage(json);
        producer.send(message);
    }

    public void stop() throws JMSException {
        producer.close();
        session.close();
        connection.close();
        executorService.shutdown();
    }

    public void stopJMS(@NotNull final BrokerService broker) throws Exception {
        broker.stop();
    }

    @SneakyThrows
    private void sync(
            @NotNull final Object object,
            @NotNull final String eventType
    ) {
        @NotNull final EventMessage eventMessage = new EventMessage(object, eventType);
        if (object.getClass().isAnnotationPresent(Table.class)) {
            @NotNull final String tableName = object.getClass().getAnnotation(Table.class).name();
            eventMessage.setTableName(tableName);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String messageJson;
        messageJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(eventMessage);
        send(messageJson);
    }

}
