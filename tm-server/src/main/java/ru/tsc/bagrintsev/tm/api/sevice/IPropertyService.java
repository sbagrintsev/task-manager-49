package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull String getDatabaseCacheHazelcastUseLiteMember();

    @NotNull String getDatabaseCacheProvConfigFileResPath();

    @NotNull String getDatabaseCacheRegionFactoryClass();

    @NotNull String getDatabaseCacheRegionPrefix();

    @NotNull String getDatabaseCacheUseMinimalPuts();

    @NotNull String getDatabaseCacheUseQueryCache();

    @NotNull
    String getDatabaseDriver();

    @NotNull String getDatabaseFormatSql();

    @NotNull String getDatabaseHbm2DdlAuto();

    @NotNull String getDatabaseShowSql();

    @NotNull String getDatabaseSqlDialect();

    @NotNull
    String getDatabaseUrl();

    @NotNull String getDatabaseUseSecondLevelCache();

    @NotNull String getDatabaseUseSqlComments();

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    Integer getPasswordHashIterations();

    @NotNull
    Integer getPasswordHashKeyLength();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
