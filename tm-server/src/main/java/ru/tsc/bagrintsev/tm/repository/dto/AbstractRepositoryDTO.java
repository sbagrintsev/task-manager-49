package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IAbstractRepositoryDTO<M> {

    @NotNull
    protected final Class<M> clazz;

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepositoryDTO(
            @NotNull final Class<M> clazz,
            @NotNull final EntityManager entityManager
    ) {
        this.clazz = clazz;
        this.entityManager = entityManager;
    }

    public void add(@NotNull final M record) {
        entityManager.persist(record);
    }

    @Override
    public void addAll(@NotNull final Collection<M> records) {
        records.forEach(this::add);
    }

    @Override
    public void clearAll() {
        @NotNull final String jpql = String.format("DELETE FROM %s", clazz.getSimpleName());
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<M> findAll() {
        @NotNull final String jpql = String.format("FROM %s", clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .getResultList();
    }

    @Override
    public @Nullable M findOneById(@NotNull String id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public long totalCount() {
        @NotNull final String jpql = String.format("SELECT count(*) FROM %s", clazz.getSimpleName());
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
