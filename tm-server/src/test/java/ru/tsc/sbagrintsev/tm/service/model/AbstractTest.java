package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.model.ITaskService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserService;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.model.AuthService;
import ru.tsc.bagrintsev.tm.service.model.ProjectService;
import ru.tsc.bagrintsev.tm.service.model.TaskService;
import ru.tsc.bagrintsev.tm.service.model.UserService;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

@Category(DBCategory.class)
public abstract class AbstractTest {

    @NotNull
    protected static IPropertyService propertyService = new PropertyService();

    @NotNull
    protected static IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected ITaskService taskService = new TaskService(connectionService);

    @NotNull
    protected IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    protected IUserService userService = new UserService(projectService, taskService, propertyService, connectionService);

    @NotNull
    protected IAuthService authService = new AuthService(userService, propertyService, connectionService);

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @BeforeClass
    public static void initConnection() {
        connectionService = new ConnectionService(propertyService);
    }

    @After
    public void destroy() {
        taskService.clearAll();
        projectService.clearAll();
        authService.clearAll();
        userService.clearAll();
    }

    @Before
    public void init() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        userService = new UserService(projectService, taskService, propertyService, connectionService);
        @NotNull final User user1 = userService.create("test1", "testPassword1");
        user1.setId("testUserId1");
        @NotNull final User user2 = userService.create("test2", "testPassword2");
        user2.setId("testUserId2");
        @NotNull final List<User> list = Arrays.asList(user1, user2);
        userService.clearAll();
        userService.set(list);
    }

}
